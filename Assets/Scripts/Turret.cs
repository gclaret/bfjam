using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject Projectile;
    [Header("How many seconds between each projectile")]
    public float FireRate = 0.5f;//
    [Header("How many projectiles are fired")]
    public float Amount = 3;
    public Transform TurretSpawner;

    [Header("Min and Max seconds for activation")]
    public float MinActivationSeconds = 5;
    public float MaxActivationSeconds = 10;

    [Header("How much time it lasts activated")]
    public float FireModeDuration = 10;
    bool isFiring = false;
    float nextFireMode;
    float secondsToActivate;

    // Start is called before the first frame update
    void Start()
    {
        nextFireMode = Time.time;
        secondsToActivate = Random.Range(MinActivationSeconds, MaxActivationSeconds);
    }

    // Update is called once per frame
    void Update()
    {
        if(Mathf.Abs( Time.time - nextFireMode) > secondsToActivate)
        {
            //Reset timers for next time
            nextFireMode = Time.time;
            secondsToActivate = Random.Range(MinActivationSeconds, MaxActivationSeconds);

            Activate();
        }
    }


    [ContextMenu("Activate Tower")]
    public void Activate()
    {
        if (isFiring)
            return;
        isFiring = true;
        StartCoroutine("Fire");
    }

    IEnumerator Fire()
    {
        var end = Time.time;
        while (isFiring)
        {
            float t = Time.time;
            while (Mathf.Abs(Time.time - t) < FireRate)
            {
                yield return new WaitForEndOfFrame();
            }
            SpawnProjectile();

            if (Mathf.Abs(Time.time - end) > FireModeDuration)
                isFiring = false;
        }
        yield return new WaitForEndOfFrame();
    }

    void SpawnProjectile()
    {
        //Randomize projectile direction
        TurretSpawner.eulerAngles = new Vector3(0, 0, Random.Range(0, 360f));
        //Instantiate bullet
        var go = Instantiate(Projectile);
        var t = go.transform;
        t.position = TurretSpawner.position;
        t.eulerAngles = new Vector3(0, 0, TurretSpawner.eulerAngles.z);
    }
}
