﻿using System;
using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerManager : MonoBehaviour
{
    [Header("Physics")]
    public float LinearDrag = 4f;
    public float Gravity = 1f;
    public float FallMultiplier = 5f;
    
    public float JumpForce;
    
    public float MovementSpeed;

    public float MaxSpeed;
    
    private Rigidbody2D _rigidbody2D;

    [SerializeField]
    private bool _isGrounded;

    private ChaosManager _chaosManager;

    private GravityManager _gravityManager;

    private float _xMovement;

    private Vector3 _originalScale;

    public bool IsAlive = false;

    void Start()
    {
        _originalScale = transform.localScale;
        _chaosManager = ChaosManager.Instance;
        _gravityManager = GravityManager.Instance;
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (_chaosManager == null || IsAlive == false)
            return;
        
        // Get the horizontal movement
        _xMovement = _chaosManager.InvertXAxis ? Input.GetAxis("Horizontal") * -1 : Input.GetAxis("Horizontal");

        CheckPlayerFacing();
        
        // See if we are jumping
        if (Input.GetKeyDown(KeyCode.UpArrow) && _isGrounded)
        {
            Jump();
        }
    }

    void FixedUpdate()
    {
        if (_rigidbody2D == null)
            return;
        
        if (_rigidbody2D.velocity.magnitude < MaxSpeed)
        {
            _rigidbody2D.AddForce(MovementSpeed * new Vector2(_xMovement, 0), ForceMode2D.Impulse);
        }

        ModifyPhysics();
    }

    void CheckPlayerFacing()
    {
        var yScale = Physics2D.gravity.y < 0 ? _originalScale.y : _originalScale.y * -1;
        
        transform.localScale = _xMovement > 0 ? new Vector3(_originalScale.x, yScale ,_originalScale.z) : _xMovement < 0 ? new Vector3(-1 * _originalScale.x, yScale ,_originalScale.z) : new Vector3(transform.localScale.x, yScale, transform.localScale.z);
    }

    void Jump()
    {
        if(_gravityManager.IsUpsideDown())
            _rigidbody2D.AddForce(new Vector2(0, -JumpForce), ForceMode2D.Impulse);
        else
            _rigidbody2D.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
    }

    void ModifyPhysics()
    {
        if (_isGrounded)
        {
            bool changingDirections = (_xMovement > 0 && _rigidbody2D.velocity.x < 0) || (_xMovement < 0 && _rigidbody2D.velocity.x > 0);

            if (Mathf.Abs(_xMovement) < 0.4f || changingDirections) {
                _rigidbody2D.drag = LinearDrag;
            } else {
                _rigidbody2D.drag = 0f;
            }

        }
        else
        {
            _rigidbody2D.gravityScale = Gravity;
            _rigidbody2D.drag = LinearDrag * 0.15f;
            if (_rigidbody2D.velocity.y < 0)
            {
                _rigidbody2D.gravityScale = Gravity * FallMultiplier;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.transform.CompareTag("Bullet") || other.transform.CompareTag("Slow"))
        {
            //DEAD!
            IsAlive = false;
            GameEvents.EndGame();
        }
        _isGrounded = IsCollidingWithGround(other);

    }

    private void OnCollisionStay2D(Collision2D other)
    {
        _isGrounded = IsCollidingWithGround(other);
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        _isGrounded = IsCollidingWithGround(other);
    }


    private void OnEnable()
    {
        GameEvents.OnGameStarts += GameStarted;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStarts -= GameStarted;
    }

    void GameStarted()
    {
        IsAlive = true;
    }



    bool IsCollidingWithGround(Collision2D other)
    {
        foreach (var contactPoint in other.contacts)
        {
            // Checks to see if the other object is below the character
            // [TODO] Add a tag check as well? Not needed yet
            var collisionDirection = contactPoint.point - _rigidbody2D.position;
            if (collisionDirection.y < 0 || collisionDirection.y > 0 && _gravityManager.IsUpsideDown())
            {
                return true;
            }
        }

        return false;
    }
}
