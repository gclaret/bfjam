﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class ChaosManager : Singleton<ChaosManager>
{
    public float MinAlarmTimer = 5;
    public float MaxAlarmTimer = 10;
    public float FastTimeScale = 2;
    public bool InvertXAxis;
    public PlayerManager Player;

    [Header("Lights")]
    public Light2D GlobalLight;

    public Light2D PlayerLight;
    
    [ContextMenu("Toggle Lights")]
    public void ToggleLights()
    {
        if(GlobalLight != null)
            GlobalLight.intensity = GlobalLight.intensity == 0 ? 1 : 0;
        
        if(PlayerLight != null)
            PlayerLight.intensity = PlayerLight.intensity == 0 ? 1 : 0;
    }

    [ContextMenu("Toggle Speed")]
    public void ToggleSpeed()
    {
        if (Time.timeScale == 1)
            Time.timeScale = FastTimeScale;
        else
            Time.timeScale = 1;
    }

    private void OnEnable()
    {
        GameEvents.OnGameStarts += GameStarted;
        GameEvents.OnGameEnded += GameEnded;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStarts -= GameStarted;
        GameEvents.OnGameEnded -= GameEnded;
    }

    void GameStarted()
    {
        StartCoroutine("UpdateAlarm");
    }

    void GameEnded()
    {

    }

    private void Start()
    {

    }

    IEnumerator UpdateAlarm()
    {
        float waitTime = Random.Range(MinAlarmTimer, MaxAlarmTimer);
        Debug.Log("Next alarm in: " + waitTime);
        float startTime = Time.time;
        while (Player.IsAlive)
        {
            //If we need to keep waiting
            if (Time.time - startTime < waitTime)
            {
                yield return new WaitForEndOfFrame();
            }
            else
            {
                //reset timers, and unleash chaos!
                waitTime = Random.Range(MinAlarmTimer, MaxAlarmTimer);
                startTime = Time.time;
                UnleashChaos();
                yield return new WaitForEndOfFrame();
            }
        }
    }

    void UnleashChaos()
    {
        int r = Random.Range(0, 4);
        switch (r)
        {
            case 0://Gravity
                GravityManager.Instance.FlipGravity();
                Debug.Log("GRAVITIY FLIP!");
                break;
            case 1://Invert Axis
                InvertXAxis = !InvertXAxis;
                Debug.Log("AXIS INVERTED!");
                break;
            case 2://Toggle lights
                ToggleLights();
                Debug.Log("LIGHT SWITCH!!");
                break;
            case 3://Toggle Speed
                ToggleSpeed();
                Debug.Log("SPEED CHANGE!!");
                break;
            default:
                break;
        }
    }
}