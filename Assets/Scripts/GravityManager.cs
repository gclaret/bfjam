﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityManager : Singleton<GravityManager>
{
    Vector3 Gravity = new Vector3(0, -9.8f, 0);

    public bool IsUpsideDown()
    {
        if (Gravity.y > 0)
            return true;
        return false;
    }

    private void Start()
    {
        Physics2D.gravity = Gravity;
    }

    private void Update()
    { 
        Physics2D.gravity = Gravity;
    }

    public void FlipGravity()
    {
        Gravity *= -1;
    }
}
