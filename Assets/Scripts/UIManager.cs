using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    public GameObject StartScreen;
    public GameObject GameScreen;
    public GameObject EndScreen;
    public Text Score;
    public Text FinalScore;
    bool gameRunning = false;
  

    void OnEnable()
    {
        GameEvents.OnPlayerScored += UpdateScore;
        GameEvents.OnGameEnded += GameEnded;

    }

    void OnDisable()
    {
        GameEvents.OnPlayerScored -= UpdateScore;
        GameEvents.OnGameEnded -= GameEnded;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartGame()
    {
        Score.text = "0";
        StartScreen.SetActive(false);
        GameScreen.SetActive(true);
        GameEvents.StartGame();
        gameRunning = true;
        StartCoroutine("ScorePoints");
    }

    IEnumerator ScorePoints()
    {
        var t = Time.time;
        while(gameRunning)
        {
            UpdateScore((int)Mathf.Abs(Time.time - t));
            yield return new WaitForEndOfFrame();
        }
    }

    void GameEnded()
    {
        gameRunning = false;
        Score.text = "DEAD";
        EndScreen.SetActive(true);
    }

    void UpdateScore(int score)
    {
        Score.text = score.ToString();
        FinalScore.text = score.ToString();
    }

    public void RestartAll()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
