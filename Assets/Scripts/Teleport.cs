using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Sprite OffSprite;
    public Sprite OnSprite;
    public SpriteRenderer Renderer;


    [ContextMenu("Activate PC")]
    public void ActivatePC()
    {
        Renderer.sprite = OnSprite;
    }

    public void DeactivatePC()
    {
        Renderer.sprite = OffSprite;
    }
}
