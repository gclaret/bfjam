using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameEvents
{
    public static Action OnGameStarts;
    public static Action OnGameEnded;
    public static Action<int> OnPlayerScored;

    public static void StartGame()
    {
        if (OnGameStarts != null)
            OnGameStarts();
    }

    public static void EndGame()
    {
        if (OnGameEnded != null)
            OnGameEnded();
    }

    public static void PlayerScored(int score)
    {
        if (OnPlayerScored != null)
            OnPlayerScored(score);
    }
}
