using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float Speed;
    bool shouldBeDestroyed = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        transform.localPosition +=  new Vector3(0, Speed/Time.timeScale, 0);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (shouldBeDestroyed)
            Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        shouldBeDestroyed = true;
    }
}
